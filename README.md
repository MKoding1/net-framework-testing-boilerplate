# NET Framework Boilerplate
Este proyecto es un boilerplate para Katas .NET 4.8 y NUnit.

## Uso
- Utilizar el IDE Visual Studio 2019 (Windows) o Jetbrains Rider (MacOS, Linux, Windows).

## Requisitos
- .NET Framework 4.8
- NUnit 3.12.0