﻿using System;
using NUnit.Framework;

namespace src.test
{
    [TestFixture]
    public class CalculatorTest
    {
        [Test]
        public void ShouldAddTwoArguments()
        {
            var calculator = new Calculator();
            
            var result = calculator.Add(1, 2);

            Assert.That(result, Is.EqualTo(3));
        }
        
        [Test]
        public void ShouldMultiplyTwoArguments()
        {
            var calculator = new Calculator();

            var result = calculator.Multiply(1, 2);

            Assert.That(result, Is.EqualTo(2));
        }
    }
}